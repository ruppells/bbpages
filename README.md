## What is it?

BBPages uses an Openresty proxy to turn a public Bitbucket repository into a static website.

## How do I use it?

I am hosting a version which I describe how to use below. If anyone abuses it tho I'll have to block that users repos or shut the whole thing down so please be considerate.

Currently I've hardcoded the config to run on my servers hostname so if you want to run/hack it yourself you'll need to change the config a little. Even better, you could make it dynamic and send me a pull request.

1. __Make sure there are no dashes (-) in your username or repository name.__
   Dashes are used by the proxy as a delimeter.
   
1. __Put your static HTML site in a Bitbucket branch named bb-pages with an index.html as your default landing page.__
   Make sure the repo is public!
   If you want to share webpages with a code repo you need to create an orphaned branch for your docs. I think the git command is `git checkout --orphan bb-pages` but I've been known to get git commands wrong so google is your friend.

1. __CNAME your hostname to {username}-{reponame}.bbpages.ruppells.io.__
   For example, I want http://testsite.ruppells.io/ to show the site in my ruppells/blog repo. To do this I create a DNS record for testsite.ruppells.io which is a CNAME to ruppells-blog.bbpages.ruppells.io. Don't try anything fancy like aliases or cnames to cnames here, I rely on that being a CNAME.

1. __Enjoy your bitbucket repo site on your custom URL.__
   You can also skip the CNAME stuff and just view it at http://{username}-{reponame}.bbpages.ruppells.io/

## Why was it written?

I wanted to use [Openresty](http://openresty.org/) to solve a problem and https://bitbucket.org/site/master/issue/2184/support-cnames-for-repositories-bb-3655 needed solving.

## Disclaimer

I don't work for Atlassian and won't support this as a product.

Use the config and my hosted version entirely at your own risk.

If the hosted version generates too much traffic or just because I feel like it I'll shut it down without warning. But, so long as it's not costing me much effort or money I'll keep it running.
